import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App';
import CommentBox from '../components/CommentBox';

it('shows a comment box', () => {
    const div = document.createElement('div');

    ReactDOM.render(<App />, div);

    // Looks inside the div
    // and checks to see if the CommentBOx is in there
    expect(div).toHaveAnInstanceOf(CommentBox);

    ReactDOM.unmountComponentAtNode(div);
});